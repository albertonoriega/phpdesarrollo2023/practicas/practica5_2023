<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="">
        <div>
            <label for="numero">Introduce número</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <?php
    $numero = 0;

    if (isset($_GET["enviar"])) {
        $numero = $_GET["numero"];

        if ($numero % 2 == 0) {
            echo "El número {$numero} es par";
        } else {
            echo "El número {$numero} es impar";
        }
    }

    ?>
</body>

</html>